EnMod-Base
----------

My default [Roots](https://github.com/jenius/roots) template, adulterated with my preferences and geared toward my tastes. Uses [Jade](https://github.com/visionmedia/jade), [Stylus](https://github.com/learnboost/stylus), and [LiveScript](https://github.com/gkz/LiveScript), along with [Axis](https://github.com/jenius/axis) and [Rupture](https://github.com/jenius/rupture) Stylus plugins.  Also uses [Lost Grid](https://github.com/corysimmons/lost)  via [PostCSS](https://github.com/postcss/postcss) out of the box!

Don't like my code?  Think I have terrible taste?  No worries! Go ahead and hop into `app.coffee` or my `package.json` and do what you must. Then, fork this and we can talk about it if it turns out my skills need a serious audit.

Installation
------------

To install: `roots template add enmod-base`    
To make default: `roots template default enmod-base`

> **Note**: This template is installed by default with roots, so this is not necessary, just serves as an example for other roots templates here

Usage
-----

`roots new project-name --base`
